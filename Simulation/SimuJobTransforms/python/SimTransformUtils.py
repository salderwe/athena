from __future__ import division
# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

## @brief Module with Digitization transform options and substep

import logging
msg = logging.getLogger(__name__)

from PyJobTransforms.trfExe import athenaExecutor

### Add Argument Methods
def addCommonSimDigArguments(parser):
    from SimuJobTransforms.simTrfArgs import addForwardDetTrfArgs, addCommonSimDigTrfArgs
    addCommonSimDigTrfArgs(parser)
    addForwardDetTrfArgs(parser)

def addCommonSimulationArguments(parser):
    from SimuJobTransforms.simTrfArgs import addCommonSimTrfArgs, addSimIOTrfArgs, addCosmicsTrfArgs, addTrackRecordArgs
    addSimIOTrfArgs(parser)
    addCommonSimTrfArgs(parser)
    addCosmicsTrfArgs(parser)
    addTrackRecordArgs(parser)

def addPureSimulationArguments(parser):
    addCommonSimulationArguments(parser)
    from SimuJobTransforms.simTrfArgs import addSim_tfArgs
    addSim_tfArgs(parser)

def addPureDigitizationArguments(parser):
    from SimuJobTransforms.simTrfArgs import addBasicDigiArgs, addPileUpTrfArgs
    addBasicDigiArgs(parser)
    addPileUpTrfArgs(parser)

def addReSimulationArguments(parser):
    from SimuJobTransforms.simTrfArgs import addCommonSimTrfArgs, addCosmicsTrfArgs, addTrackRecordArgs, addSim_tfArgs, addReSimulationArgs
    addCommonSimTrfArgs(parser)
    addCosmicsTrfArgs(parser)
    addTrackRecordArgs(parser)
    addCommonSimDigArguments(parser)
    addSim_tfArgs(parser)
    addReSimulationArgs(parser)

def addSimulationArguments(parser):
    addCommonSimDigArguments(parser)
    addPureSimulationArguments(parser)

def addAtlasG4Arguments(parser):
    addCommonSimDigArguments(parser)
    addPureSimulationArguments(parser)

def addHITSMergeArguments(parser):
    from SimuJobTransforms.simTrfArgs import addHITSMergeArgs
    addHITSMergeArgs(parser)

def addDigitizationArguments(parser):
    addCommonSimDigArguments(parser)
    addPureDigitizationArguments(parser)

def addHITSValidArguments(parser):
    from SimuJobTransforms.simTrfArgs import addHITSValidArgs, addCommonSimDigTrfArgs
    addHITSValidArgs(parser)
    addCommonSimDigTrfArgs(parser)

def addRDOValidArguments(parser):
    from SimuJobTransforms.simTrfArgs import addRDOValidArgs, addCommonSimDigTrfArgs
    addRDOValidArgs(parser)
    addCommonSimDigTrfArgs(parser)

### Add Sub-step Methods
## @brief Add ISF transform substep
#  @param overlayTransform If @c True use the tweaked version of in/outData for an overlay job
def addSimulationSubstep(executorSet, overlayTransform = False):
    TRExe = athenaExecutor(name = 'TRtoHITS',
                           skeletonCA = 'SimuJobTransforms.ISF_Skeleton',
                           substep = 'simTRIn', tryDropAndReload = False, perfMonFile = 'ntuple.pmon.gz',
                           inData=['EVNT_TR'],
                           outData=['HITS','NULL'] )
    executorSet.add(TRExe)
    SimExe = athenaExecutor(name = 'EVNTtoHITS',
                                   skeletonCA = 'SimuJobTransforms.ISF_Skeleton',
                                   substep = 'sim', tryDropAndReload = False, perfMonFile = 'ntuple.pmon.gz',
                                   inData=['NULL','EVNT'],
                                   outData=['EVNT_TR','HITS','NULL'] )
    if overlayTransform:
        from PyJobTransforms.trfUtils import releaseIsOlderThan
        if releaseIsOlderThan(20,3):
            SimExe.inData = [('EVNT', 'BS_SKIM')]
        else:
            SimExe.inData = [('EVNT','TXT_EVENTID')]
        SimExe.outData = ['HITS']
        SimExe.inputDataTypeCountCheck = ['EVNT']
    executorSet.add(SimExe)


def addReSimulationSubstep(executorSet):
    SimExe = athenaExecutor(name = 'ReSim',
                            skeletonCA = 'SimuJobTransforms.ReSimulation_Skeleton',
                            substep = 'rsm',
                            tryDropAndReload = False,
                            perfMonFile = 'ntuple.pmon.gz',
                            inData=['HITS'],
                            outData=['HITS_RSM'],
                            inputDataTypeCountCheck = ['HITS'] )
    executorSet.add(SimExe)


def addAtlasG4Substep(executorSet):
    executorSet.add(athenaExecutor(name = 'AtlasG4TfTRIn',
                                   skeletonCA = 'SimuJobTransforms.G4AtlasAlg_Skeleton',
                                   substep = 'simTRIn', tryDropAndReload = False,
                                   inData=['EVNT_TR'],
                                   outData=['HITS','NULL'] ))
    executorSet.add(athenaExecutor(name = 'AtlasG4Tf',
                                   skeletonCA = 'SimuJobTransforms.G4AtlasAlg_Skeleton',
                                   substep = 'sim', tryDropAndReload = False,
                                   inData=['NULL','EVNT'],
                                   outData=['EVNT_TR','HITS','NULL'] ))


def addConfigurableSimSubstep(executorSet, confName, extraSkeleton, confSubStep, confInData, confOutData, confExtraRunargs, confRuntimeRunargs):
    executorSet.add(athenaExecutor(name = confName, skeletonFile = extraSkeleton + ['SimuJobTransforms/skeleton.EVGENtoHIT_MC12.py'],
                                   substep = confSubStep, tryDropAndReload = False,
                                   inData = confInData,
                                   outData = confOutData, extraRunargs = confExtraRunargs, runtimeRunargs = confRuntimeRunargs ))


def addStandardHITSMergeSubstep(executorSet):
    executorSet.add(athenaExecutor(name = 'HITSMerge', substep="hitsmerge",
                                   skeletonCA = 'SimuJobTransforms.HITSMerge_Skeleton',
                                   tryDropAndReload = False, inputDataTypeCountCheck = ['HITS']))


def addDigitizationSubstep(executorSet, in_reco_chain=False):
    executorSet.add(athenaExecutor(name = 'HITtoRDO',
                                   skeletonCA='SimuJobTransforms.HITtoRDO_Skeleton',
                                   substep = 'h2r', tryDropAndReload = False,
                                   inData = ['HITS'], outData = ['RDO','RDO_FILT'],
                                   onlyMPWithRunargs = [
                                       'inputLowPtMinbiasHitsFile',
                                       'inputHighPtMinbiasHitsFile',
                                       'inputCavernHitsFile',
                                       'inputBeamHaloHitsFile',
                                       'inputBeamGasHitsFile'
                                   ] if in_reco_chain else None))


def addSimValidationSubstep(executorSet):
    executorSet.add(athenaExecutor(name = 'SimValidation',
                                   skeletonCA='SimuJobTransforms.HITtoHIST_SIM_Skeleton',
                                   inData = ['HITS'], outData = ['HIST_SIM'],))


def addDigiValidationSubstep(executorSet):
    executorSet.add(athenaExecutor(name = 'DigiValidation',
                                   skeletonCA='SimuJobTransforms.RDOtoHIST_DIGI_Skeleton',
                                   inData = ['RDO'], outData = ['HIST_DIGI'],))

### Append Sub-step Methods
def appendSimulationSubstep(trf):
    executor = set()
    addSimulationSubstep(executor)
    trf.appendToExecutorSet(executor)

def appendAtlasG4Substep(trf):
    executor = set()
    addAtlasG4Substep(executor)
    trf.appendToExecutorSet(executor)

def appendConfigurableSimTRInSubstep(trf, confName = 'AtlasG4TfTRIn',
                                     extraSkeleton = [], confSubstep = 'simTRIn',
                                     confInData=['EVNT_TR'],
                                     confOutData=['HITS','NULL'],
                                     confExtraRunargs=None, confRuntimeRunargs=None ):
    executor = set()
    addConfigurableSimSubstep(executor, confName, extraSkeleton, confSubstep, confInData, confOutData, confExtraRunargs, confRuntimeRunargs )
    trf.appendToExecutorSet(executor)

def appendConfigurableSimSubstep(trf, confName = 'AtlasG4Tf',
                                 extraSkeleton = [], confSubstep = 'sim',
                                 confInData=['NULL','EVNT'],
                                 confOutData=['EVNT_TR','HITS','NULL'],
                                 confExtraRunargs=None, confRuntimeRunargs=None ):
    executor = set()
    addConfigurableSimSubstep(executor, confName, extraSkeleton, confSubstep, confInData, confOutData, confExtraRunargs, confRuntimeRunargs )
    trf.appendToExecutorSet(executor)

def appendHITSMergeSubstep(trf):
    executor = set()
    addStandardHITSMergeSubstep(executor)
    trf.appendToExecutorSet(executor)

def appendDigitizationSubstep(trf):
    executor = set()
    addDigitizationSubstep(executor)
    trf.appendToExecutorSet(executor)

#def appendSimValidationSubstep(trf):
#   executor = set()
#  addSimValidationSubstep(executor)
# trf.appendSimVAlidationSubstep(executor)

#def appendDigiValidationSubstep(trf):
#   executor = set()
#  addDigiValidationSubstep(executor)
# trf.appendDigiVAlidationSubstep(executor)
